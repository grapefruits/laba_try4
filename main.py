#Даниленко Олексій
text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged."

# Розділити текст на окремі слова
words = text.split()

# Перетворення тексту в нижній регістр
lowercase_text = text.lower()

# Заміна слова
updated_text = text.replace("Lorem", "MYBABY")

# Вивести результати
print("Слова з тексту:", words)
print()
print("Текст у нижньому регістрі:", lowercase_text)
print()
print("Замінений текст:", updated_text)
print()

# Функції для наступного студента:
# Підрахувати кількість входжень слова "text" у текст
# Знайти позицію першого входження слова "industry" у тексті
# Перетворити першу букву кожного речення у тексті на велику літеру
#Владислав Цюх:

# 1)Підраховуємо кількість входжень слова "text" у текст
text_count = text.count("text")

# 2)Знаходимо позицію першого входження слова "industry" у тексті
industry_position = text.find("industry")

# 3)Перетворюємо першу букву кожного речення у тексті на велику літеру
capitalized_text = text.capitalize()

# Вивести результати
print("Кількість входжень слова 'text':", text_count)
print()
print("Позиція першого входження слова 'industry':", industry_position)
print()
print("Текст з великою першою буквою в кожному реченні:", capitalized_text)
print()

# Функції для наступного студента:
# Знайти індекс останнього входження слова "the" у тексті
# Визначити кількість символів у тексті
# Визначити, чи складається текст лише з цирф і літер

# Поливаний Денис

# 1) Знайти індекс останнього входження слова "the" у тексті:
last_the = text.rfind("the")

# 2) Визначити кількість символів у тексті:
lenght = len(text)

# 3) Визначити, чи складається текст лише з цирф і літер:
alphanum = text.isalnum()

print("Індекс останнього входження 'the' у тексті:", last_the)
print()
print("Кількість символів у тексті:", lenght)
print()
print("Чи складається текст лише з цифр і літер: ", alphanum)
print()

